This is a pretty simple mod for selecting specific player components to be either visible or not visible.

How to install:
First install .NET Scripthook, then copy PlayerComponentsSelector.net.dll and PlayerComponentsSelector.ini to the scripts folder in your GTA IV directory.

Controls:
Right-CTRL + C = Toggle the menu for selecting the components
NumPad8 = Scroll up in the menu
NumPad2 = Scroll down in the menu
NumPad5 = Select the component
NumPad/ = Change visibility of the components / set them all back to visible (this key works no matter if the menu is open or not)
-Keys are changeable in the ini file

Bugs:
Doesn't work in MP (at least with the standard model).
When you die, all your components will be set back to visible

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
