﻿using GTA;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace PlayerComponentsSelector
{
    class Component
    {
        public string Name;
        public PedComponent ComponentIndex;

        public Component(string name, PedComponent componentIndex)
        {
            Name = name;
            ComponentIndex = componentIndex;
        }
    }

    public class PlayerComponentsSelector : Script
    {
        enum LastDone
        {
            SET_AS_SELECTED_VISIBILITY = 1,
            SET_AS_ALL_VISIBLE = 2
        }

        bool menuOpen = false, useHoldKey;
        int selectedItem = 0;
        bool[] componentsVisibility = new bool[] { true, true, true, true, true, true, true, true, true, true };
        Component[] components = new Component[] { 
            new Component("Hair", PedComponent.Hair), 
            new Component("Head", PedComponent.Head),
            new Component("Teeth", PedComponent.Teeth),
            new Component("Torso", PedComponent.UpperBody), 
            new Component("Bags", PedComponent.Bags),
            new Component("Jacket", PedComponent.Jacket), 
            new Component("Hands", PedComponent.Hand), 
            new Component("Unknown body part", PedComponent.Unknown_SUS2),
            new Component("Legs", PedComponent.LowerBody),
            new Component("Feet", PedComponent.Feet)
        };
        Keys menuTogglePressKey, menuToggleHoldKey, menuSelectKey, menuUpKey, menuDownKey, changeVisibilityKey;
        LastDone lastDone = LastDone.SET_AS_SELECTED_VISIBILITY;

        public PlayerComponentsSelector()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("menu_toggle_press_key", "keys", Keys.C);
                Settings.SetValue("menu_toggle_hold_key", "keys", Keys.RControlKey);
                Settings.SetValue("use_hold_key", "keys", true);
                Settings.SetValue("menu_select_key", "keys", Keys.NumPad5);
                Settings.SetValue("menu_scroll_up_key", "keys", Keys.NumPad8);
                Settings.SetValue("menu_scroll_down_key", "keys", Keys.NumPad2);
                Settings.SetValue("change_component_visibilities_key", "keys", Keys.Divide);
                Settings.Save();
            }

            menuTogglePressKey = Settings.GetValueKey("menu_toggle_press_key", "keys", Keys.C);
            menuToggleHoldKey = Settings.GetValueKey("menu_toggle_hold_key", "keys", Keys.RControlKey);
            useHoldKey = Settings.GetValueBool("use_hold_key", "keys", true);
            menuSelectKey = Settings.GetValueKey("menu_select_key", "keys", Keys.NumPad5);
            menuUpKey = Settings.GetValueKey("menu_scroll_up_key", "keys", Keys.NumPad8);
            menuDownKey = Settings.GetValueKey("menu_scroll_down_key", "keys", Keys.NumPad2);
            changeVisibilityKey = Settings.GetValueKey("change_component_visibilities_key", "keys", Keys.Divide);

            KeyDown += PlayerComponentsSelector_KeyDown;
            PerFrameDrawing += PlayerComponentsSelector_PerFrameDrawing;
            //Interval = 500;
            //Tick += PlayerComponentsSelector_Tick;
        }

        //void PlayerComponentsSelector_Tick(object sender, EventArgs e)
        //{
        //    throw new NotImplementedException();
        //}

        void PlayerComponentsSelector_PerFrameDrawing(object sender, GraphicsEventArgs e)
        {
            if (menuOpen)
            {
                Color color;
                float y = 20;

                for (int i = 0; i < components.Length; i++)
                {
                    if (componentsVisibility[i])
                    {
                        if (i == selectedItem)
                        {
                            color = Color.DarkBlue;
                        }
                        else
                        {
                            color = Color.Blue;
                        }
                    }
                    else
                    {
                        if (i == selectedItem)
                        {
                            color = Color.Gray;
                        }
                        else
                        {
                            color = Color.White;
                        }
                    }

                    e.Graphics.DrawText(components[i].Name, 20, y, color);
                    y += 30;
                }
            }
        }

        void PlayerComponentsSelector_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if ((!useHoldKey || isKeyPressed(menuToggleHoldKey)) && e.Key == menuTogglePressKey)
            {
                menuOpen = !menuOpen;
                if (menuOpen)
                {
                    for (int i = 0; i < components.Length; i++)
                    {
                        Player.SetComponentVisibility(components[i].ComponentIndex, true);
                    }
                }
            }

            if (e.Key == changeVisibilityKey)
            {
                bool visibility = true;

                if (lastDone == LastDone.SET_AS_ALL_VISIBLE)
                {
                    lastDone = LastDone.SET_AS_SELECTED_VISIBILITY;
                }
                else if (lastDone == LastDone.SET_AS_SELECTED_VISIBILITY)
                {
                    lastDone = LastDone.SET_AS_ALL_VISIBLE;
                }

                for (int i = 0; i < components.Length; i++)
                {
                    if (lastDone == LastDone.SET_AS_SELECTED_VISIBILITY) visibility = true;
                    else visibility = componentsVisibility[i];

                    Player.SetComponentVisibility(components[i].ComponentIndex, visibility);
                }
            }

            if (menuOpen)
            {
                if (e.Key == menuSelectKey)
                {
                    componentsVisibility[selectedItem] = !componentsVisibility[selectedItem];
                }
                else if (e.Key == menuUpKey)
                {
                    if (selectedItem > 0) selectedItem--;
                }
                else if (e.Key == menuDownKey)
                {
                    if (selectedItem < components.Length - 1) selectedItem++;
                }
            }
        }

        private void Subtitle(string text, int time = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", text, time, 1);
        }
    }
}
